FROM golang

WORKDIR /app

# Copy the Go modules files
COPY go.mod go.sum ./

# Download the Go module dependencies
RUN go mod download

# Copy the source code
COPY . .

# Build the Go program
RUN go build -o main .

#Run unit test
RUN go test *.go
