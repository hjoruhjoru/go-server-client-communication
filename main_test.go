package main

import (
	"github.com/stretchr/testify/assert"
	"sync"
	"testing"
)

func TestClient1(t *testing.T) {
	input := make(chan []int)
	output := make(chan float64)

	var wg sync.WaitGroup
	wg.Add(1)

	go client1(input, output, &wg)

	input <- []int{12233, 2443, 2333}
	result := <-output
	assert.Equal(t, 5669.666666666667, result)

	input <- []int{}
	close(input)

}

func TestClient2(t *testing.T) {
	input := make(chan []int)
	output := make(chan int)

	var wg sync.WaitGroup
	wg.Add(1)

	go client2(input, output, &wg)

	input <- []int{12233, 2443, 2333}
	result := <-output
	assert.Equal(t, 2443, result)

	input <- []int{}
	close(input)
}

func TestClient3(t *testing.T) {
	input := make(chan []int)
	output := make(chan int)

	var wg sync.WaitGroup
	wg.Add(1)

	go client3(input, output, &wg)

	input <- []int{12233, 2333, 2333}
	result := <-output
	assert.Equal(t, 2333, result)

	input <- []int{}
	close(input)
}