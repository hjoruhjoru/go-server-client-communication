# go-server-client-communication

## Description
The Server process will monitor keyboard input from the user, and send the data to all Client processes. The clients will calculate and put result to server.

## Getting started

### get library
```
go get
```
### run server and client
```
go run main.go
```
### run unit test
```
go test *.go
```
