package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"sort"
	"strconv"
	"strings"
	"sync"
)

func main() {
	// Create communication channels
	clientInput := make(chan []int)

	client1Output := make(chan float64)
	client2Output := make(chan int)
	client3Output := make(chan int)

	// Create a WaitGroup
	var wg sync.WaitGroup

	// Increment the WaitGroup counter for each goroutine
	wg.Add(4)

	// Start the server
	go server(clientInput, client1Output, client2Output, client3Output, &wg)

	// Start Client1
	go client1(clientInput, client1Output, &wg)

	// Start Client2
	go client2(clientInput, client2Output, &wg)

	// Start Client3
	go client3(clientInput, client3Output, &wg)

	// Wait for all goroutines to finish
	wg.Wait()
}

func server(clientInput chan<- []int, client1Output <-chan float64, client2Output <-chan int, client3Output <-chan int, wg *sync.WaitGroup) {
	clientNum := 3
	defer wg.Done()

	reader := bufio.NewReader(os.Stdin)
	for {
		fmt.Print("Enter integers separated by spaces: ")
		input, err := reader.ReadString('\n')
		if err != nil {
			log.Fatal(err)
		}
		if len(input) > 0 && input[0] == 81 {
			close(clientInput)
			fmt.Println("Channel close")
			break
		}

		// Parse the input
		numbersStr := strings.Fields(input)
		numbers := make([]int, 0, len(numbersStr))
		for _, numStr := range numbersStr {
			num, err := strconv.Atoi(strings.TrimSpace(numStr))
			if err != nil {
				log.Printf("Invalid input: %v", err)
				continue
			}
			numbers = append(numbers, num)
		}

		// Send data to clients
		for i := 0; i < clientNum; i++ {
			clientInput <- numbers
		}
		mean := <-client1Output
		median := <-client2Output
		mode := <-client3Output
		fmt.Printf("Client1: Mean value is %.2f\n", mean)
		fmt.Printf("Client2: Median value is %d\n", median)
		fmt.Printf("Client3: Mode value is %d\n", mode)
	}
}

func client1(input <-chan []int, output chan<- float64, wg *sync.WaitGroup) {
	defer wg.Done()
	for {
		num := <-input
		if len(num) != 0 {
			sum := 0
			for _, val := range num {
				sum += val
			}
			mean := float64(sum) / float64(len(num))
			output <- mean
		} else {
			close(output)
			break
		}
	}
}

func client2(input <-chan []int, output chan<- int, wg *sync.WaitGroup) {
	defer wg.Done()
	for {
		num := <-input
		if len(num) != 0 {
			sort.Ints(num)
			median := calculateMedian(num)
			output <- median
		} else {
			close(output)
			break
		}
	}
}

func calculateMedian(data []int) int {
	n := len(data)
	if n%2 == 0 {
		mid := n / 2
		return (data[mid-1] + data[mid]) / 2
	} else {
		return data[n/2]
	}
}

func client3(input <-chan []int, output chan<- int, wg *sync.WaitGroup) {
	defer wg.Done()
	counts := make(map[int]int)
	for {
		num := <-input
		if len(num) != 0 {
			for _, val := range num {
				counts[val]++
			}
			mode := calculateMode(counts)
			output <- mode
		} else {
			close(output)
			break
		}
	}
}

func calculateMode(counts map[int]int) int {
	maxCount := 0
	mode := 0
	for num, count := range counts {
		if count > maxCount {
			maxCount = count
			mode = num
		}
	}
	return mode
}
